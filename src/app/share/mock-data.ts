import { Flight } from './../flight';
export class MockData {

  public static ExampleData: Flight[] =
  [
    {
      fullName: "Test User",
      from: "CountryA",
      to: "CountryB",
      type: "One way",
      adults: 0,
      departure: new Date("2022-01-01"),
      children: 0,
      infants: 0,
      arrival: new Date("2022-01-01")
    }
  ]
}
